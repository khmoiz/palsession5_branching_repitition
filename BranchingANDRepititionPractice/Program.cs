﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BranchingANDRepititionPractice
{
    class Program
    {
        //----- ONLY PUT CODE WHERE THE COMMENTS ARE - DO NOT CHANGE ANY CODE THAT HAS ALREADY BEEN WRITTEN -----\\
        //NOTE: Click the '+' icons on the left to expand methods and scopes

        static void Main(string[] args)
        {
            Console.WriteLine("-----BRANCH BEGINNER-----");
            Console.WriteLine();

            //CALL BranchBeginner, passing it "Mike" as a parameter - write the output to the console - ie. Console.WriteLine(BranchBeginner("Mike"));
            //CALL BranchBeginner, passing it "James" as a parameter - write the output to the console

            Console.WriteLine();
            Console.WriteLine("-----BRANCH VETERAN-----");
            Console.WriteLine();

            //CALL BranchVeteran, passing it two integer values, with both being the same value - write the output to the console
            //CALL BranchVeteran, passing it two integer values, with the first value being larger than the second - write the output to the console
            //CALL BranchVeteran, passing it two integer values, with the second value being larger than the first - write the output to the console

            Console.WriteLine();
            Console.WriteLine("-----BRANCH MASTER-----");
            Console.WriteLine();

            //CALL BranchMaster, passing it 10 as a parameter value
            //CALL BranchMaster, passing it 6 as a parameter value
            //CALL BranchMaster, passing it 0 as a parameter value

            Console.WriteLine();
            Console.WriteLine("-----BRANCH EXPERT-----");
            Console.WriteLine();

            //CALL BranchExpert, passing it 110 as a parameter value

            Console.WriteLine();

            //CALL BranchExpert, passing it -10 as a paramterer value

            Console.WriteLine();

            //CALL BranchExpert, passing it 5 as a parameter value

            Console.WriteLine();
            Console.WriteLine("-----LOOP BEGINNER-----");
            Console.WriteLine();

            //CALL LoopBeginner

            Console.WriteLine();
            Console.WriteLine("-----LOOP VETERAN-----");
            Console.WriteLine();

            //CALL LoopVeteran, passing it 5 as a parameter value

            Console.WriteLine();

            //CALL LoopVeteran, passing it 10 as a parameter value

            Console.WriteLine();

            //CALL LoopVeteran, passing it 0 as a parameter value

            Console.WriteLine();
            Console.WriteLine("-----LOOP MASTER-----");
            Console.WriteLine();

            //CALL LoopMaster

            Console.ReadKey();
        }

        #region BRANCHING EXERCISES (4)

        /// <summary>
        /// Starting out with branches? Create this method and you will start your journey!
        /// </summary>
        /// <param name="name">string value that will be passed when calling the method</param>
        /// <returns>True or False, depending on the string value passed as a parameter</returns>
        static bool BranchBeginner(string name)
        {
            //WRITE AN IF/ELSE BLOCK
            //IF the name entered is "Mike", return true. ELSE, return false


        }

        /// <summary>
        /// Now that you know the basics about branching, create this method to enhance your branching skills!
        /// </summary>
        /// <param name="numOne">any integer value</param>
        /// <param name="numTwo">any integer value</param>
        /// <returns>a string value which will be descriptive of which number was greater, or a tie if there was one</returns>
        static string BranchVeteran(int numOne, int numTwo)
        {
            //WRITE AN IF/ELSE IF/ELSE BLOCK
            //IF the first number is greather than the second number, return "firstnum"

            //OTHERWISE, IF the second number is greater than the first number, return "secondnum"

            //ELSE, return "tie"
         
        }

        /// <summary>
        /// Now that you are experienced with branches, create this method to perfect your skill
        /// </summary>
        /// <param name="num">any integer value between 1 and 10</param>
        static void BranchMaster(int num)
        {
            //DECLARE a string variable, initialize it to "zero". Name it whatever you wish

            //WRITE A SWITCH BLOCK, with 'num' in the parenthesis

            //CREATE ten cases, each case being a number (creat case 1, case 2, etc. up until 10
            //EACH case should set the variable's (declared above) value to the literal value of the current case
            //FOR EXAMPLE, for case 1, the variable should be assigned the value "one"

            //Have cases 2,3 and 4 as fall-through cases, in which they will all set he variable to "fall-through"

            //CREATE the default case, have it write "default" to the console, then have a 'break' statement

            //REMEMBER: EACH case requires a 'break' statement after the code within the case

            //AFTER THE SWITCH BLOCK, write the value of the variable to the console

        }

        /// <summary>
        /// Want to become a branch expert? Create this method and you will be an expert with branching!
        /// </summary>
        /// <param name="num">any integer value</param>
        static void BranchExpert(int num)
        {
            //CREATE A NESTED IF BRANCH WITH THE FOLLOWING 'IF' STATEMENTS:
            //Each 'IF' statement will be nested with the one before it

            //IF 'num' (passed as parameter) is greater than 0, write "greater than 0" to the console
            //ELSE, write "less than 0" to the console

            //*NESTED* IF 'num' is less than 100, write "less than 100" to the console
            //ELSE, write "greater than 100" to the console

            //*NESTED* IF 'num' is less than 50, write "less than 50" to the console
            //ELSE, write "greater than 50" to the console

            //*NESTED* IF 'num' is less than 25, write "less than 25" to the console
            //ELSE, write "greater than 25" to the console

            //*NESTED* IF 'num' is less than 10, write "less than 10" to the console
            //ELSE, write "greater than 10" to the console

        }

        #endregion 

        #region REPITITION EXERCISES (3)

        /// <summary>
        /// Want to create a loop? Creating this method will give you a great start to your looping adventures!
        /// </summary>
        static void LoopBeginner()
        {
            //WRITE A FOR LOOP that loops 10 times, and each time it loops, write "hi" to the console

        }

        /// <summary>
        /// Now that you know 'for' loops, broaden your skills and complete this 'while' loop method!
        /// </summary>
        /// <param name="num">an integer value</param>
        static void LoopVeteran( int num)
        {
            //CREATE an integer counter variable, name it whatever you wish, initialize it to 0

            //WRITE A WHILE LOOP that runs as long as the counter variable is less than 'num'
            //Each time it loops, write "bye" to the console, and add 1 to the counter variable

        }

        /// <summary>
        /// Now that you are a veteran in the looping world, learn about 'foreach' and create this method!
        /// </summary>
        static void LoopMaster()
        {
            //DECLARE AND DEFINE a LIST with five different integer values, name the list whatever you like
            //HINT: should be one line of code.

            //WRITE A FOREACH LOOP that loops through EACH number in the list, and write the current number to the console
        }

        #endregion

    }
}
